# Author: Herson Esquivel-Vargas
####################################################################################################
# Loop
from global_variables import *
from pyshark.packet.fields import LayerField

####################################################################################################
def loop_state_transitions(pkt, data):
    """
    Looks for pointers related to the current loop object. Those pointers are standard BACnet
    properties. If any of them is found, we jump to the corresponding state.

    Keyword arguments
    pkt -- BACnet protocol packet
    data -- list representation of the BACapp layer in pkt. As we read the list gets shorter [1:]
    """
    if len(data) == 0:                                 # No more data available
        if current_object.get_oid() != None and referenced_object.get_oid() == None:
            insert_node(pkt, SINGLE)
        return ("end", pkt, data)                      # goto end state
    
    head = data[0]
    new_state = "loop"
    if not isinstance(head, LayerField):
        return ("loop", pkt, data[1:])
    
    if 'Property Identifier:' in head.show:
        prop_id = head.raw_value[2:]                   # '2913' -> '13' in hex -> 19 in dec
        # TODO: test prop_ids > 255
        if prop_id == '13':                            # if prop == controlled-variable-reference
            new_state = "0x13"
        elif prop_id == '3c':                          # if prop == manipulated-variable-reference
            new_state = "0x3c"
        elif prop_id == '6d':                          # if prop == setpoint-reference
            new_state = "0x6d"
        elif prop_id == '58':                          # if prop == priority-for-writing
            new_state = "0x58_loop"
    elif 'ObjectIdentifier:' in head.show:             # If it is an object...
        return ("read", pkt, data)                     #  goto "read" with the same data
    return(new_state, pkt, data[1:])


# TODO: the following pointers could reach other physical devices. This case is currently not
# supported
####################################################################################################
def prop_0x13_transitions(pkt, data):                  # controlled-variable-reference
    global referenced_object
    
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      # goto end state
    
    head = data[0]
    new_state = "0x13"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])

    if 'ObjectIdentifier:' in head.show:
        obj_id = head.raw_value[2:]
        referenced_object.set_oid(obj_id)
    elif head.hex_value == 79:                         # }[4] or 0x4f, is a closing tag for 0x4e
        if referenced_object.get_oid() != None:
            insert_node(pkt, CONTROLLED_VARIABLE_REFERENCE)
        else:
            insert_node(pkt, SINGLE)
        new_state = "loop"
    return (new_state, pkt, data[1:])

####################################################################################################
def prop_0x3c_transitions(pkt, data):                  # manipulated-variable-reference
    global referenced_object
    
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      # goto end state
    
    head = data[0]
    new_state = "0x3c"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])

    if 'ObjectIdentifier:' in head.show:
        obj_id = head.raw_value[2:]
        referenced_object.set_oid(obj_id)
    elif head.hex_value == 79:                         # }[4] or 0x4f, is a closing tag for 0x4e
        if referenced_object.get_oid() != None:
            insert_node(pkt, MANIPULATED_VARIABLE_REFERENCE)
        else:
            insert_node(pkt, SINGLE)
        new_state = "loop"
    return (new_state, pkt, data[1:])

####################################################################################################
def prop_0x6d_transitions(pkt, data):                  # setpoint-reference
    global referenced_object
    
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      # goto end state
    
    head = data[0]
    new_state = "0x6d"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])

    if 'ObjectIdentifier:' in head.show:
        obj_id = head.raw_value[2:]
        referenced_object.set_oid(obj_id)
    elif head.hex_value == 79:                         # }[4] or 0x4f, is a closing tag for 0x4e
        if referenced_object.get_oid() != None:
            insert_node(pkt, SETPOINT_REFERENCE)
        else:
            insert_node(pkt, SINGLE)
        new_state = "loop"
    return (new_state, pkt, data[1:])

####################################################################################################
def prop_0x58_loop_transitions(pkt, data):             # priority-for-writing (pfw)
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      # goto end state

    head = data[0]
    new_state = "0x58_loop"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])

    if 'priority-for-writing:' in head.show:
        pfw = int(head.raw_value[2:], 16)              # pfw is in hex form. E.g., '2101'
        insert_pfw(pkt, pfw)
        new_state = "loop"
    return (new_state, pkt, data[1:])
