#!/usr/bin/python3
# Author: Herson Esquivel-Vargas
#
import pyshark
import sys
import argparse
from loop import *
from eventenrollment import *
from genericobject import *
from notificationclass import *
from schedule import *
from trendlog import *

####################################################################################################
def main(argv):
    """
    This method initializes the FSM, loads the pcap file to memory, and triggers the analysis.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="pcap file to be analyzed.")
    args = parser.parse_args()
        
    fill_in_FSM_states()

    # The current filter keeps:
    # BACnet packets whose service is (ReadProperty OR ReadPropertyMultiple)
    # AND are ACKs
    graph_filter = (
        '(bacapp.confirmed_service == 14 or bacapp.confirmed_service==12)'
        ' and bacapp.type == 3'
    )
    cap = pyshark.FileCapture(args.file, display_filter=graph_filter)
    cap.apply_on_packets(packet_analyzer)
    cap.close()

####################################################################################################
def fill_in_FSM_states():
    """ Populates the FSM with state names and the corresponding handlers (function names) """
    fsm.add_state("start", start_transitions)
    fsm.add_state("read", read_transitions)
    fsm.add_state("generic", generic_state_transitions)
    fsm.add_state("gen0x11", generic_prop_0x11_transitions)
    fsm.add_state("loop", loop_state_transitions)
    fsm.add_state("0x13", prop_0x13_transitions)
    fsm.add_state("0x3c", prop_0x3c_transitions)
    fsm.add_state("0x6d", prop_0x6d_transitions)
    fsm.add_state("0x58_loop", prop_0x58_loop_transitions)
    fsm.add_state("schedule", schedule_state_transitions)
    fsm.add_state("0x36", prop_0x36_transitions)
    fsm.add_state("0x36_obj", prop_0x36_obj_transitions)
    fsm.add_state("0x58_schedule", prop_0x58_schedule_transitions)
    fsm.add_state("trend-log", trend_log_state_transitions)
    fsm.add_state("0x84", prop_0x84_transitions)
    fsm.add_state("0x84_obj", prop_0x84_obj_transitions)
    fsm.add_state("notification-class", notification_class_state_transitions)
    fsm.add_state("0x66", prop_0x66_transitions)
    fsm.add_state("0x56", prop_0x56_transitions)
    fsm.add_state("event-enrollment", event_enrollment_state_transitions)
    fsm.add_state("ee0x11", ee_prop_0x11_transitions)
    fsm.add_state("0x4e", prop_0x4e_transitions)
    fsm.add_state("0x4e_obj", prop_0x4e_obj_transitions)
    fsm.add_state("end", None, end_state=1)
    fsm.set_start("start")

####################################################################################################
def packet_analyzer(pkt):
    """ This method analyzes every packet through the FSM. """
    global current_sadr, current_eth_sadr, current_object, referenced_object
    current_object.clear()
    referenced_object.clear()
    try:
        fsm.run(pkt)
    except:
        pass

####################################################################################################
def start_transitions(pkt, data):
    """
    This FSM handler is executed when there is yet to find what is the current object under 
    analysis. The most important task here is to clear up the global variables:
    current_object and referenced_object

    Keyword arguments
    pkt -- current packet under analysis, which could have info about several BACnet objects
    data -- list representation of the BACapp layer packet gotten as follows:
            bacapp_layer_packet._get_all_fields_with_alternates()
    """
    global current_object, referenced_object

    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      #  goto end state
    
    current_object.clear()
    referenced_object.clear()
    return ("read", pkt, data)

####################################################################################################
def read_transitions(pkt, data):
    global current_object, referenced_object
    
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "read"

    if 'ObjectIdentifier:' in head.show:               # E.g. 'ObjectIdentifier: loop, 1'
        current_object.clear()
        referenced_object.clear()
        
        obj_id = head.raw_value[2:]                    # Just the oid. E.g., '03000001'
        current_object.set_oid(obj_id)
        obj_type = current_object.get_type()           # standard object type identifier
        
        if obj_type == 9:
            new_state = "event-enrollment"
        elif obj_type == 12:
            new_state = "loop"
        elif obj_type == 15:
            new_state = "notification-class"
        elif obj_type == 17:
            new_state = "schedule"
        elif obj_type == 20:
            new_state = "trend-log"
        else:
            new_state = "generic"
    return (new_state, pkt, data[1:])    


####################################################################################################
def get_description(dev_id):
    global descriptions
    try:
        des = descriptions[dev_id]
        des = des.lower()
        des = re.sub('[^a-zA-Z]',' ',des)
        return des
    except:
        return False


####################################################################################################
if __name__ == '__main__':
    main(sys.argv)
