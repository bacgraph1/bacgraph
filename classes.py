# Author: Herson Esquivel-Vargas
#
import binascii
from neo4j import GraphDatabase

####################################################################################################
class FiniteStateMachine:
    """"
    FSM class which can be initialized with arbitrary states and function handlers for each
    of them.
    """
    ################################################################################################
    def __init__(self):
        """ Initialization of the object properties """
        self.handlers = {}
        self.start_state = None
        self.end_states = []

    ################################################################################################
    def add_state(self, name, handler, end_state=0):
        """
        Adds states to the FSM.

        Keyword arguments:
        name -- state name
        handler -- function to be executed whenever the state is reached
        end_state -- flag that indicates whether this is an end_state
        """
        self.handlers[name] = handler
        if end_state:
            self.end_states.append(name)

    ################################################################################################
    def set_start(self, name):
        self.start_state = name

    ################################################################################################
    def __str__(self):
        return str(len(self.handlers)) + "states"
        
    ################################################################################################
    def run(self, pkt):
        """
        Each BACnet packet will pass through this method which updates the FSM state

        Keyword arguments:
        pkt -- BACnet protocol packet
        """
        try:
            handler = self.handlers[self.start_state]
        except:
            raise InitializationError("Call .set_start() before .run()")
        
        data = pkt['BACAPP']._get_all_fields_with_alternates() # Extracts APDU data
        while True:
            (new_state, pkt, data) = handler(pkt, data)
            if new_state in self.end_states:
                break
            else:
                handler = self.handlers[new_state]


####################################################################################################
class BACnetObject:
    """
    This class models the basic properties of BACnet objects
    """
    ################################################################################################
    def __init__(self, oid=None):
        self.oid = oid                                 # oid: object id in the form '03000001'
        self.device = None
        self.instance_number = None
        self.object_type = None
        self.description = None
        self.BACaddr = None
        self.time = None

    ################################################################################################
    def __str__(self):
        return str(self.device) + "_" + str(self.oid)

    ################################################################################################
    # TODO: static method?
    def get_type(self):
        """
        Keyword returns
        Numeric value of the BACnet object. E.g., 12 for loop objects
        """
        try:
            return int.from_bytes(binascii.a2b_hex(self.oid), 'big') >> 22
        except:
            return -1

    ################################################################################################
    def set_oid(self, oid=None):
        """
        If the instance_number and object_type are given we can compute the oid
        According to the BACnet standard the 32 bits are used as:
        10 bits for the object_type
        22 bits for the instance_number
        """
        if oid == None:
            if (self.instance_number != None and self.object_type != None):
                tmp = self.object_type << 22              # Move 22 positions <--
                tmp = tmp | self.instance_number          #  and "or" the instance_number
                tmp_bytes = tmp.to_bytes(4, 'big')        # to_bytes(...) = b'\x02@\x00\x14'
                ascii_bytes = binascii.b2a_hex(tmp_bytes) # b'02400014'
                self.oid = ascii_bytes.decode('utf-8')    # '02400014'
        else:
            self.oid = oid

    ################################################################################################
    def get_oid(self):
        return self.oid

    ################################################################################################
    #TODO: Shall I remove the elif statement since device objects are not supposed to reach this
    #      function anymore?
    def set_device(self, device=None):
        """ Sets the device identifier to a BACnet object """
        if device != None:
            self.device = device
        elif self.get_type() == 8:
            self.device = (int.from_bytes(binascii.a2b_hex(self.oid), 'big') &
                           int.from_bytes(b'\x00\x3f\xff\xff', 'big'))

    def get_device(self):
        return self.device

    def set_instance_number(self, instance_number):
        self.instance_number = instance_number

    def get_instance_number(self):
        return self.instance_number

    def set_object_type(self, object_type):
        self.object_type = object_type

    def get_object_type(self):
        return self.object_type

    def set_description(self, description):
        self.description = description

    def get_description(self):
        return self.description

    def set_BACaddr(self, bacaddr):
        self.BACaddr = bacaddr

    def get_BACaddr(self):
        return self.BACaddr

    def set_time(self, time):
        self.time = time

    def get_time(self):
        return self.time
    
    def clear(self):
        self.oid = None
        self.device = None
        self.description = None
        self.instance_number = None
        self.object_type = None

####################################################################################################
class NeoDriver(object):
    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password), encrypted=False)

    def __del__(self):
        self.close()
        
    def close(self):
        self.driver.close()
        
    ################################################################################################
    @staticmethod
    def _get_dev_id(tx, bacaddress):
        query = ("MATCH (n:BACnetDevice) "
                 "WHERE n.bac_addr = $bacaddr "
                 "RETURN n.dev_id")
        result = tx.run(query, bacaddr = bacaddress)
        return result.single()

    ################################################################################################
    @staticmethod
    def _insert_pfw(tx, node, pfw, pkt):
        query = ("MATCH (n:BACnetObject {node_id:$node}) "
                 "SET n.pfw=$prio, n.last_activity_time=$pkt_time")
        r = tx.run(query, node=node.__str__(), prio=pfw, pkt_time=pkt.frame_info.time_epoch)
        tx.commit()

    ################################################################################################
    @staticmethod
    def _insert_priority(tx, node, priority, pkt):
        query = ("MATCH (n:BACnetObject {node_id:$node}) "
                 "SET n.priority=$prio, n.last_activity_time=$pkt_time")
        r = tx.run(query, node=node.__str__(), prio=priority, pkt_time=pkt.frame_info.time_epoch)
        tx.commit()
    
    ################################################################################################
    @staticmethod
    def _set_objects_link(tx, a, b, link_type, pkt):
        query1 = ("MERGE (a:BACnetObject {node_id:$a_node_id}) "
                  "ON CREATE "
                  "SET a.node_id=$a_node_id, a.dev_id=$a_dev_id, a.oid=$a_oid, "
                  "    a.obj_type=$a_obj_type, a.creation_time=$pkt_time, "
                  "    a.last_activity_time=$pkt_time "
                  "ON MATCH "
                  "SET a.last_activity_time=$pkt_time ")
        query2 = ("MERGE (b:BACnetObject {node_id:$b_node_id}) "
                  "ON CREATE "
                  "SET b.node_id=$b_node_id, b.dev_id=$b_dev_id, b.oid=$b_oid, "
                  "    b.obj_type=$b_obj_type, b.creation_time=$pkt_time, "
                  "    b.last_activity_time=$pkt_time "
                  "ON MATCH "
                  "SET b.last_activity_time=$pkt_time ")
        query3 = ("MATCH (a:BACnetObject {node_id:$a_node_id}),(b:BACnetObject {node_id:$b_node_id}) "
                  "MERGE (a)-[r:"+link_type+"]->(b)"
                  "ON CREATE "
                  "SET r.creation_time=$pkt_time, r.last_activity_time=$pkt_time "
                  "ON MATCH "
                  "SET r.last_activity_time=$pkt_time")
        tx.run(query1,
               a_node_id=a.__str__(), a_dev_id=int(a.get_device()), a_oid=a.get_oid(),
               a_obj_type=a.get_type(), pkt_time=pkt.frame_info.time_epoch)
        tx.run(query2,
               b_node_id=b.__str__(), b_dev_id=int(b.get_device()), b_oid=b.get_oid(),
               b_obj_type=b.get_type(), pkt_time=pkt.frame_info.time_epoch)
        tx.run(query3,
               a_node_id=a.__str__(), b_node_id=b.__str__(),
               pkt_time=pkt.frame_info.time_epoch)
        tx.commit()

    ################################################################################################
    #TODO: this method can be used by _set_objects_link to reduce some lines of code.
    @staticmethod
    def _create_single_object(tx, a, pkt):
        query1 = ("MERGE (a:BACnetObject {node_id:$a_node_id}) "
                  "ON CREATE "
                  "SET a.node_id=$a_node_id, a.dev_id=$a_dev_id, a.oid=$a_oid, "
                  "    a.obj_type=$a_obj_type, a.creation_time=$pkt_time, "
                  "    a.last_activity_time=$pkt_time "
                  "ON MATCH "
                  "SET a.last_activity_time=$pkt_time ")
        tx.run(query1,
               a_node_id=a.__str__(), a_dev_id=int(a.get_device()), a_oid=a.get_oid(),
               a_obj_type=a.get_type(), pkt_time=pkt.frame_info.time_epoch)
        tx.commit()
