# BACgraph
Author: Herson Esquivel-Vargas

License: GPLv3

BACgraph reads BACnet protocol traffic (ASHRAE, ANSI, and ISO 16484-5 standard) and stores the
observed BACnet objects in a Neo4J database including the references between them.
The process is fully automated and leverages the explicit pointers available between standard objects.

The code is written in python3 and freely available under the GNU/GPLv3 license.


## Citation
If you use this software, please cite it as:

> BACgraph: Automatic Extraction of Object Relationships in the BACnet Protocol. Herson Esquivel-Vargas, Marco Caselli and Andreas Peter. 51st Annual IEEE/IFIP International Conference on Dependable Systems and Networks, DSN (Industry Track) 2021, Taipei, Taiwan, June 21-24, 2021.


## Installation
### Requirements

* Pyshark library
```
$ pip3 install pyshark
```

* Neo4j python driver
```
$ pip3 install neo4j
```

* Neo4j database
 * Go to https://neo4j.com/download-center/#community and download the latest version available.
Please note, however, that this code was last tested on version 4.2.3 Community Edition.

 * Extract
```
$ tar -xf neo4j-community-4.2.3-unix.tar.gz
```
 * To run Neo4j as a console application, use:
```
<NEO4J_HOME>/bin/neo4j console
```

 * Go to http://localhost:7474/ and change the default password

### BACgraph

* Download
```
$ git clone <THIS_REPOSITORY>
```

* Move to the repository directory and write your database password in _global_variables.py_ file
```
...
# Database parameters
uri="bolt://localhost:7687"
user="neo4j"
pwd= ""                       <- Your password here
db = NeoDriver(uri, user, pwd)
...
```

* Set the mode of operation in _global_variables.py_
```
...
strict = False
...
```
 * Strict: BACgraph will store in the database only those objects whose device identifier is known.
 Strict mode is required in real environments to fully identify the objects in the infrastructure.
 To run BACgraph in strict mode it is recommended to run first _device-object-finder.py_.
 This will add to the database the devices observed in the network, and then _BACgraph.py_
 will link the observed BACnet objects with those devices.
```
$ ./device-object-finder.py file.pcap
```
 
 * Non-strict (default): BACgraph will try to store BACnet objects along with their corresponding
 device identifier (if known). Otherwise, BACgraph will use a default device identifier (4194303).
 Non-strict mode is useful to try out small pcap files mostly for testing purposes.


## Execution
Run _BACgraph.py_ passing a pcap file as a parameter.
```
$ ./BACgraph.py file.pcap
```

## Examples
We illustrate the execution of BACgraph using pcaps from a third-party repository.
All the examples shown here use the _non-strict_ operation mode.
The numbers inside each node indicate the object type number:
Analog Input=0, Analog Output=1, etc.

To visualize the graphs after the analysis of each pcap, go to http://localhost:7474/
and run
```
neo4j$ MATCH (n) RETURN n
```

### Loop
File from [kargs.net](http://kargs.net/captures/loop2.cap).
This file contains a Loop object detailing its setpoint, sensor, and actuator.
However, it is possible to quickly identify a misconfiguration since the setpoint (sr) and sensor (cvr)
references are pointing to the same object (Analog Input instance 0).

![Graph comprised of 3 nodes](images/loop2-kargs.png)

### Schedule
File from [kargs.net](http://kargs.net/captures/BACnetL_SchedRPM.pcapng).
This file contains a Schedule object without any references on the _list-of-object-property-references_
property. As a consequence, BACgraph simply adds the Schedule object to the database.

![Single Schedule object](images/schedule-kargs.png)

### Generic Objects
File from [kargs.net](http://kargs.net/captures/TrendLogMultipleReadRangeSimple.pcap).
BACgraph found 40 BACnet objects of diverse types in this pcap.
One device object (type number 8) has a pointer to a notification class object (type number 15).
Moreover, one Analog Input (type number 0) and and Binary Input (type number 3)
share a common notification class object.
Other objects observed without any specific relationship observed were also added to the database.

![Diverse object types](images/generic-NC.png)
