# Author: Herson Esquivel-Vargas
####################################################################################################
# Trend log
from global_variables import *
from pyshark.packet.fields import LayerField

####################################################################################################
def trend_log_state_transitions(pkt, data):
    """
    Looks for pointers related to the current trend_log object. Those pointers are standard BACnet
    properties. If any of them is found, we jump to the corresponding state.

    Keyword arguments
    pkt -- BACnet protocol packet
    data -- list representation of the BACapp layer in pkt. As we read the list gets shorter [1:]
    """
    if len(data) == 0:                                 # No more data available
        if current_object.get_oid() != None and referenced_object.get_oid() == None:
            insert_node(pkt, SINGLE)
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "trend-log"
    if not isinstance(head, LayerField):
        return ("trend-log", pkt, data[1:])
    
    if 'Property Identifier:' in head.show:
        prop_id = head.raw_value[2:]                   # '2913' -> '13' in hex -> 19 in dec
        # TODO: test prop_ids > 255
        if prop_id == '84':                            # if prop == log-device-object-property
            new_state = "0x84"
    elif 'ObjectIdentifier:' in head.show:             # If it is an object...
        return ("read", pkt, data)                     #  goto "read" with the same data
    return(new_state, pkt, data[1:])

####################################################################################################
def prop_0x84_transitions(pkt, data):                  # log-device-object-property
    global referenced_object
    
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "0x84"
    if not isinstance(head, LayerField):
        return ("0x84", pkt, data[1:])

    if 'ObjectIdentifier:' in head.show:
        obj_id = head.raw_value[2:]
        tmp_object = BACnetObject()
        tmp_object.set_oid(obj_id)
        if tmp_object.get_type() != 8:                 # not device object?
            new_state = "0x84_obj"                     #  goto new state with same data
            referenced_object.set_oid(obj_id)          # populate referenced_object properties

    elif (head.hex_value == 79 or head.hex_value == 94):# Early closing tag or error:  }[4] or {[5]
        if current_object.get_oid() != None:
            insert_node(pkt, SINGLE)
        new_state = "trend-log"                        #  goto previous state
    return (new_state, pkt, data[1:])

####################################################################################################
def prop_0x84_obj_transitions(pkt, data):
    global referenced_object
    
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "0x84_obj"
    if not isinstance(head, LayerField):
        return ("0x84_obj", pkt, data[1:])

    if 'ObjectIdentifier:' in head.show:
        obj_id = head.raw_value[2:]
        tmp_object = BACnetObject()
        tmp_object.set_oid(obj_id)
        if tmp_object.get_type() == 8:                 # device object?
            tmp_object.set_device()                    # set the device_id and copy the device_id
            referenced_object.set_device(tmp_object.get_device())
            insert_node(pkt, LOG_DEVICE_OBJECT_PROPERTY)
            new_state = "0x84"
        else:
            new_state = "0x84"                         # previous state
            return (new_state, pkt, data)              #  with the same data

    elif head.hex_value == 79:                         # }[4] or 0x4f, is a closing tag for 0x4e
        if referenced_object.get_oid() != None:
            insert_node(pkt, LOG_DEVICE_OBJECT_PROPERTY)
        else:
            insert_node(pkt, SINGLE)
        new_state = "trend-log"
    return (new_state, pkt, data[1:])
