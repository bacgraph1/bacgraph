# Author: Herson Esquivel-Vargas
####################################################################################################
# Generic object
from global_variables import *
from pyshark.packet.fields import LayerField

####################################################################################################
def generic_state_transitions(pkt, data):
    if len(data) == 0:                                 # No more data available
        if current_object.get_oid() != None:
            insert_node(pkt, SINGLE)
        return ("end", pkt, data)                      # goto end state
    
    head = data[0]
    new_state = "generic"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])

    if 'Property Identifier:' in head.show:
        prop_id = head.raw_value[2:]                   # '294e' -> '4e' in hex
        # TODO: test prop_ids > 255
        if prop_id == '11':                            # if prop == notification-class
            new_state = "gen0x11"
    elif 'ObjectIdentifier:' in head.show:             # If it is an object...
        if current_object.get_oid() != None:
            insert_node(pkt, SINGLE)
        return ("read", pkt, data)                     #  goto "read" with the same data
    return (new_state, pkt, data[1:])

####################################################################################################
def generic_prop_0x11_transitions(pkt, data):          # notification-class
    global referenced_object
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "gen0x11"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])

    if 'notification-class:' in head.show:
        tmp_str = head.raw_value[2:]                   # '2112' -> '12' in hex
        inst_number = int(tmp_str, 16)                 # 18 in decimal
        referenced_object.clear()
        referenced_object.set_object_type(15)          # Notification class object-type-id is 15
        referenced_object.set_instance_number(inst_number) # the observed instance number
        referenced_object.set_oid()                    # Automatically computes the oid

    elif head.hex_value == 79 or head.hex_value == 94: # closing tag or error:  }[4] or {[5]
        if referenced_object.get_oid() != None:
            insert_node(pkt, NOTIFICATION_CLASS)
        else:
            insert_node(pkt, SINGLE)
        new_state = "generic"

    return (new_state, pkt, data[1:])
