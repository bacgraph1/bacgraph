#!/usr/bin/python3
# Author: Herson Esquivel-Vargas
#
import binascii
import pyshark
import sys
from neo4j import GraphDatabase

####################################################################################################
class NeoDriver(object):
    """
    This class creates a driver from the GraphDatabase class in the neo4j module.
    If you do not have the neo4j module, please try the following command:
    $ pip3 install neo4j
    """
    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password), encrypted=False)

    def __del__(self):
        self.close()
        
    def close(self):
        self.driver.close()
        
    @staticmethod
    def _create_device(tx, bac_address, instance_num, oid, current_time):
        """
        Inserts a BACnetDevice node in the database.
        The goal is to have a link between device_identifiers and bac_addresses.
        This is important because we often observe only the bac_address in network packets.
        When can only insert BACnet objects in the database if the device they belong to is known.
        Otherwise the database is filled with objects without a full identification which leads to
        duplication of nodes.

        Keyword arguments:
        tx -- neo4j transaction object
        bac_address -- Network Protocol Data Unit (NPDU) unique address of the device
        instance_num -- Application Protocol Data Unit (APDU) unique identifier of the device
        oid -- BACnet object identifier of the device object
        current_time -- Packet timestamp
        """
        result = tx.run("MERGE (a:BACnetDevice {bac_addr:$bac_addr}) "
                        "ON CREATE "
                        "SET a.bac_addr = $bac_addr, a.dev_id = $dev_id, a.node_id = $node_id, "
                        "    a.creation_time=$pkt_time, a.last_activity_time=$pkt_time "
                        "ON MATCH "
                        "SET a.last_activity_time=$pkt_time "
                        "RETURN id(a)",
                        bac_addr=bac_address, dev_id=instance_num,
                        node_id=str(instance_num)+'_'+str(oid), pkt_time=current_time)
        return result.single()


####################################################################################################
def main(argv):
    if len(argv) != 2:
        print("pcap file required")
        sys.exit(1)
    pcap_process(argv[1])


####################################################################################################
def pcap_process(pcap_file_name):
    """
    Loads the pcap file to start the analysis.
    This function uses the pyshark module. If you do not have it, please try the following command:
    $ pip3 install pyshark

    Keyword arguments:
    pcap_file_name -- pcap file path
    """
    # The current filter keeps:
    # BACnet packets whose service is (ReadProperty OR ReadPropertyMultiple)
    # AND are ACKs AND contain objects of type 'device'
    pcap_filter = (
        '(bacapp.confirmed_service == 14 or bacapp.confirmed_service==12)'
        ' and bacapp.type == 3 and bacapp.objectType == 8'
    )
    cap = pyshark.FileCapture(pcap_file_name, display_filter=pcap_filter)
    cap.apply_on_packets(parse_packet)
    cap.close()


####################################################################################################
def create_object_id(object_id, instance_number):
    """
    Creates an object id using the format defined in the BACnet standard (ASHRAE, ANSI, and 
    ISO 16484-5 standard)

    Keyword arguments:
    object_id -- decimal value of the object type. 10 bit integer.
    instance_number -- decimal value of the instance_number. 22 bit integer.
    
    return -- Hex-string of 4 bytes merging 10+22 bit parameters as specified by the BACnet standard
    """
    oid = ((object_id << 22) | instance_number)      # Integer form of the oid (e.g. 74099373)
    oid_bytes = oid.to_bytes(4, 'big')               # Bytes form of the oid (e.g. b'\x04j\xaa\xad')
    oid_hex = binascii.hexlify(oid_bytes)            # Hex form of the oid (e.g. b'046aaaad')
    return oid_hex.decode('utf-8')                   # Hex-string form of the oid (e.g. '046aaaad')


####################################################################################################
def parse_packet(pkt):
    """
    Pyshark's apply_on_packets() call-back function will execute this function for each network packet
    """
    dev_sadr = pkt['BACNET'].get_field('bacnet.sadr_eth')
    obj_type = pkt['BACAPP'].get_field('bacapp.objecttype') # obj_type is a LayerFieldsContainer ('8')

    if not (dev_sadr and obj_type.int_value == 8):
        return

    instance_num = pkt['BACAPP'].instance_number
    oid = create_object_id(obj_type.int_value, instance_num.base16_value)

    session = db.driver.session()
    tx = session.begin_transaction()
    r = db._create_device(tx, dev_sadr, instance_num, oid, pkt.frame_info.time_epoch)
    tx.commit()
    session.close()


####################################################################################################
# Global variables
uri = "bolt://localhost:7687"
user = "neo4j"
pwd = ""                                             # <- Set your password here
db = NeoDriver(uri, user, pwd)


####################################################################################################
if __name__ == '__main__':
    main(sys.argv)




    
