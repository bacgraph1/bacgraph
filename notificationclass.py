# Author: Herson Esquivel-Vargas
####################################################################################################
# Notification Class
from global_variables import *
from pyshark.packet.fields import LayerField

####################################################################################################
def notification_class_state_transitions(pkt, data):
    """
    Looks for pointers related to the current notification-class object. Those pointers are standard
    BACnet properties. If any of them is found, we jump to the corresponding state.

    Keyword arguments
    pkt -- BACnet protocol packet
    data -- list representation of the BACapp layer in pkt. As we read the list gets shorter [1:]
    """
    if len(data) == 0:                                 # No more data available
        if current_object.get_oid() != None and referenced_object.get_oid() == None:
            insert_node(pkt, SINGLE)
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "notification-class"
    if not isinstance(head, LayerField):
        return ("notification-class", pkt, data[1:])

    if 'Property Identifier:' in head.show:
        prop_id = head.raw_value[2:]                   # '2966' -> '66' in hex -> 102 in dec
        # TODO: test prop_ids > 255
        if prop_id == '66':                            # if prop == recipient-list
            new_state = "0x66"
        elif prop_id == '56':                          # if prop == priority
            new_state = "0x56"
    elif 'ObjectIdentifier:' in head.show:             # If it is an object...
        return ("read", pkt, data)                     #  goto "read" with the same data
    return(new_state, pkt, data[1:])

####################################################################################################
def prop_0x66_transitions(pkt, data):                  # recipient-list
    global referenced_object
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "0x66"
    if not isinstance(head, LayerField):
        return ("0x66", pkt, data[1:])
    
    if 'ObjectIdentifier:' in head.show:
        obj_id = head.raw_value[2:]
        referenced_object.set_oid(obj_id)
    elif head.hex_value == 79:                         # }[4] or 0x4f, is a closing tag for 0x4e
        if referenced_object.get_oid() != None:
            insert_node(pkt, RECIPIENT_LIST)
        else:
            insert_node(pkt, SINGLE)
        new_state = "notification-class"
    return (new_state, pkt, data[1:])

####################################################################################################
def prop_0x56_transitions(pkt, data):                  # priority
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      # goto end state

    # priority is an array of 3 integers
    head1 = data[0]
    head2 = data[2]
    head3 = data[4]

    priority = [-666] * 3
    
    new_state = "0x56"
    if not isinstance(head1, LayerField):
        return (new_state, pkt, data[1:])

    if 'priority:' in head1.show and 'priority:' in head2.show and 'priority:' in head3.show:
        priority[0] = int(head1.raw_value[2:], 16)     # priority is in hex form. E.g., '21ff'
        priority[1] = int(head2.raw_value[2:], 16)     # priority is in hex form. E.g., '21ff'
        priority[2] = int(head3.raw_value[2:], 16)     # priority is in hex form. E.g., '21ff'
        insert_priority(pkt, str(priority))
        new_state = "notification-class"
    elif head1.hex_value == 79:                         # }[4] or 0x4f, is a closing tag for 0x4e
        new_state = "notification-class"
        
    return (new_state, pkt, data[1:])

