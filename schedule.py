# Author: Herson Esquivel-Vargas
####################################################################################################
# Schedule
from global_variables import *
from pyshark.packet.fields import LayerField

####################################################################################################
def schedule_state_transitions(pkt, data):
    """
    Looks for pointers related to the current schedule object. Those pointers are standard BACnet
    properties. If any of them is found, we jump to the corresponding state.

    Keyword arguments
    pkt -- BACnet protocol packet
    data -- list representation of the BACapp layer in pkt. As we read the list gets shorter [1:]
    """
    if len(data) == 0:                                 # No more data available
        if current_object.get_oid() != None and referenced_object.get_oid() == None:
            insert_node(pkt, SINGLE)
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "schedule"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])
    
    if 'Property Identifier:' in head.show:
        prop_id = head.raw_value[2:]                   # '2913' -> '13' in hex -> 19 in dec
        # TODO: test prop_ids > 255
        if prop_id == '36':                            # if prop == list-of-object-property-referen
            new_state = "0x36"
        elif prop_id == '58':                          # if prop == priority-for-writing
            new_state = "0x58_schedule"
    elif 'ObjectIdentifier:' in head.show:             # If it is an object...
        return ("read", pkt, data)                     #  goto "read" with the same data
    return(new_state, pkt, data[1:])

####################################################################################################
def prop_0x36_transitions(pkt, data):                  # list-of-object-property-references
    global referenced_object

    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "0x36"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])

    if 'ObjectIdentifier:' in head.show:
        obj_id = head.raw_value[2:]
        tmp_object = BACnetObject()
        tmp_object.set_oid(obj_id)
        if tmp_object.get_type() != 8:                 # not device object?
            new_state = "0x36_obj"                     #  goto new state
            referenced_object.set_oid(obj_id)          # populate referenced_object properties

    elif (head.hex_value == 79 or head.hex_value == 94):# Early closing tag or error:  }[4] or {[5]
        if current_object.get_oid() != None:
            insert_node(pkt, SINGLE)
        new_state = "schedule"                         #  goto previous state
    return (new_state, pkt, data[1:])

####################################################################################################
def prop_0x36_obj_transitions(pkt, data):
    global referenced_object
    
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "0x36_obj"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])

    if 'ObjectIdentifier:' in head.show:
        new_state = "0x36"
        obj_id = head.raw_value[2:]
        tmp_object = BACnetObject()
        tmp_object.set_oid(obj_id)
        if tmp_object.get_type() == 8 :                # device object?
            tmp_object.set_device()                    #  set the device_id and copy the device_id
            referenced_object.set_device(tmp_object.get_device())
            insert_node(pkt, LIST_OF_OBJECT_PROPERTY_REFERENCES)
        else:
            if referenced_object.get_oid() != None:
                insert_node(pkt, LIST_OF_OBJECT_PROPERTY_REFERENCES)
                return (new_state, pkt, data)          #  with the same data

    elif head.hex_value == 79:                         # }[4] or 0x4f, is a closing tag for 0x4eo
        if referenced_object.get_oid() != None:
            insert_node(pkt, LIST_OF_OBJECT_PROPERTY_REFERENCES)
        else:
            insert_node(pkt, SINGLE)
        new_state = "schedule"
    return (new_state, pkt, data[1:])

####################################################################################################
def prop_0x58_schedule_transitions(pkt, data):         # priority-for-writing
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      # goto end state

    head = data[0]
    new_state = "0x58_schedule"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])

    if 'priority-for-writing:' in head.show:
        pfw = int(head.raw_value[2:], 16)              # pfw is in hex form. E.g., '2101'
        insert_pfw(pkt, pfw)
        new_state = "schedule"
    return (new_state, pkt, data[1:])
