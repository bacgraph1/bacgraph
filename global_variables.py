# Author: Herson Esquivel-Vargas
#
import sys
from classes import BACnetObject
from classes import FiniteStateMachine
from classes import NeoDriver

# Finite State Machine instance
fsm = FiniteStateMachine()

# Types of edges
SINGLE = 0
CONTROLLED_VARIABLE_REFERENCE = 0x13
MANIPULATED_VARIABLE_REFERENCE = 0x3c
SETPOINT_REFERENCE = 0x6d
NOTIFICATION_CLASS = 0x11
OBJECT_PROPERTY_REFERENCE = 0x4e
RECIPIENT_LIST = 0x66
LOG_DEVICE_OBJECT_PROPERTY = 0x84
LIST_OF_OBJECT_PROPERTY_REFERENCES = 0x36
EXCEPTION_SCHEDULE = 0x26
NOTIFICATION_CLASS = 0x11

# Variables that ara manipulated throughout the FSM walk
current_object = BACnetObject()
referenced_object = BACnetObject()

# Database parameters
uri = "bolt://localhost:7687"
user = "neo4j"
pwd = ""                                                # <- Set your password here
db = NeoDriver(uri, user, pwd)

# Operation mode
"""
If BACgraph runs on strict mode, a valid device id must be found to store the new node in the DB.
If not running on strict mode, this method assigns a default device id (4194303).
"""
# TODO: get this flag from the command line.
strict = False


####################################################################################################
def get_dev_id(pkt):
    """
    Returns the unique BACnet device identifier from the BACnet NPDU address.
    This address linking is useful because many discovered objects do not include the device id but
    only the NPDU address.
    """
    current_sadr = pkt['BACNET'].get_field('bacnet.sadr_eth')
    session = db.driver.session()
    tx = session.begin_transaction()
    r = db._get_dev_id(tx, current_sadr)
    tx.commit()
    session.close()
    try:
        return r[0]
    except:
        if not strict:
            """
            From the standard:
            Object properties that contain values whose datatype is BACnetObjectIdentifier may
            use 4194303 as the instance number to indicate that the property is not initialized.
            """
            # When not running on strict mode, store objects using a default oid.
            return 4194303
        else:
            return None

####################################################################################################
def insert_node(pkt, edge_type):
    # TODO: validate completeness of both current and referenced BACnet objects
    current_object.set_device()
    if current_object.get_device() ==  None:                        # Try to assign the device_id
        current_object.set_device(get_dev_id(pkt))

    if edge_type == SINGLE:
        if current_object.get_device() != None:
            session = db.driver.session()
            tx = session.begin_transaction()
            db._create_single_object(tx, current_object, pkt)
            session.close()
            return

    if referenced_object.get_device() ==  None:                     # if destination is None
        referenced_object.set_device(current_object.get_device())   # copy from source
        
    # Ready to insert into the database
    if current_object.get_device() != None and referenced_object.get_device() !=  None:
        edge_name = "undefined"
        if edge_type == CONTROLLED_VARIABLE_REFERENCE:
            edge_name = "CVR"
        elif edge_type == MANIPULATED_VARIABLE_REFERENCE:
            edge_name = "MVR"
        elif edge_type == SETPOINT_REFERENCE:
            edge_name = "SR"
        elif edge_type == NOTIFICATION_CLASS:
            edge_name = "NC"
        elif edge_type == OBJECT_PROPERTY_REFERENCE:
            edge_name = "OPR"
        elif edge_type == RECIPIENT_LIST:
            edge_name = "RL"
        elif edge_type == LOG_DEVICE_OBJECT_PROPERTY:
            edge_name = "LDOP"
        elif edge_type == LIST_OF_OBJECT_PROPERTY_REFERENCES:
            edge_name = "LOOPR"
        elif edge_type == EXCEPTION_SCHEDULE:
            edge_name = "ES"

        session = db.driver.session()
        tx = session.begin_transaction()
        db._set_objects_link(tx, current_object, referenced_object, edge_name, pkt)
        session.close()
        
####################################################################################################
def insert_pfw(pkt, pfw):
    # TODO: validate completeness of current BACnet object
    current_object.set_device()
    if current_object.get_device() ==  None:           # Try to assign the device_id
        current_object.set_device(get_dev_id(pkt))

    if current_object.get_device() != None and pfw >= 0 and pfw <= 16:
        session = db.driver.session()
        tx = session.begin_transaction()
        db._insert_pfw(tx, current_object, pfw, pkt)
        session.close()

####################################################################################################
def insert_priority(pkt, priority):
    # TODO: validate completeness of current BACnet object
    current_object.set_device()
    if current_object.get_device() ==  None:           # Try to assign the device_id
        current_object.set_device(get_dev_id(pkt))

    if current_object.get_device() != None:
        session = db.driver.session()
        tx = session.begin_transaction()
        db._insert_priority(tx, current_object, priority, pkt)
        session.close()


