# Author: Herson Esquivel-Vargas
####################################################################################################
# Event enrollment
from global_variables import *
from pyshark.packet.fields import LayerField

####################################################################################################
def event_enrollment_state_transitions(pkt, data):
    """
    Looks for pointers related to the current event_enrollment object. Those pointers are standard
    BACnet properties. If any of them is found, we jump to the corresponding state.

    Keyword arguments
    pkt -- BACnet protocol packet
    data -- list representation of the BACapp layer in pkt. As we read the list gets shorter [1:]
    """
    if len(data) == 0:                                 # No more data available
        if current_object.get_oid() != None and referenced_object.get_oid() == None:
            insert_node(pkt, SINGLE)
        return ("end", pkt, data)                      #  goto end state

    head = data[0]
    new_state = "event-enrollment"
    if not isinstance(head, LayerField):
        return ("event-enrollment", pkt, data[1:])
    
    if 'Property Identifier:' in head.show:
        prop_id = head.raw_value[2:]                   # '294e' -> '4e' in hex
        # TODO: test prop_ids > 255
        if prop_id == '4e':                            # if prop == object-property-reference
            new_state = "0x4e"
        elif prop_id == '11':                          # if prop == notification-class
            new_state = "ee0x11"
    elif 'ObjectIdentifier:' in head.show:             # If it is an object...
        return ("read", pkt, data)                     #  goto "read" with the same data
    return(new_state, pkt, data[1:])

# TODO: the following pointers could reach other physical devices. This case is currently not
# supported
####################################################################################################
def ee_prop_0x11_transitions(pkt, data):               # notification-class
    global referenced_object
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "ee0x11"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])

    if 'notification-class:' in head.show:
        tmp_str = head.raw_value[2:]                   # '2112' -> '12' in hex
        inst_number = int(tmp_str, 16)                 # 18 in decimal
        referenced_object.clear()
        referenced_object.set_object_type(15)          # Notification class object-type-id is 15
        referenced_object.set_instance_number(inst_number) # the observed instance number
        referenced_object.set_oid()                    # Automatically computes the oid

    elif head.hex_value == 79:                         # }[4] or 0x4f, is a closing tag for 0x4e
        if referenced_object.get_oid() != None:
            insert_node(pkt, NOTIFICATION_CLASS)
        else:
            insert_node(pkt, SINGLE)
        new_state = "event-enrollment"

    return(new_state, pkt, data[1:])

####################################################################################################
def prop_0x4e_transitions(pkt, data):                  # object-property-reference
    global referenced_object
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "0x4e"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])

    if 'ObjectIdentifier:' in head.show:
        obj_id = head.raw_value[2:]
        tmp_object = BACnetObject()
        tmp_object.set_oid(obj_id)
        if tmp_object.get_type() != 8:                 # not device object?
            new_state = "0x4e_obj"                     #  goto new state with same data
            referenced_object.set_oid(obj_id)          # populate referenced_object properties
    return (new_state, pkt, data[1:])

####################################################################################################
def prop_0x4e_obj_transitions(pkt, data):
    global referenced_object
    if len(data) == 0:                                 # No more data available
        return ("end", pkt, data)                      #  goto end state
    
    head = data[0]
    new_state = "0x4e_obj"
    if not isinstance(head, LayerField):
        return (new_state, pkt, data[1:])

    if 'ObjectIdentifier:' in head.show:
        obj_id = head.raw_value[2:]
        tmp_object = BACnetObject()
        tmp_object.set_oid(obj_id)
        if tmp_object.get_type() == 8:                 # device object?
            tmp_object.set_device()                    # set the device_id and copy the device_id
            referenced_object.set_device(tmp_object.get_device())
            insert_node(pkt, OBJECT_PROPERTY_REFERENCE)
            new_state = "0x4e"
        else:
            new_state = "0x4e"                         # previous state
            return (new_state, pkt, data)              #  with the same data

    elif head.hex_value == 79:                         # }[4] or 0x4f, is a closing tag for 0x4e
        if referenced_object.get_oid() != None:
            insert_node(pkt, OBJECT_PROPERTY_REFERENCE)
        else:
            insert_node(pkt, SINGLE)
        new_state = "event-enrollment"
    return (new_state, pkt, data[1:])
